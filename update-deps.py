#!/usr/bin/env python3

import json
import os
import subprocess

def read_dependencies(filename, project):
    with open(filename) as f:
        deps = json.load(f)
    return deps.get(project, [])

def clone_repo(repo_url, dirname):
    print(f"cloning {repo_url}")
    owd = os.getcwd()
    if not os.path.exists(dep):
        subprocess.check_call(["git", "clone", repo_url, dirname])
    else:
        # figure out default branch
        # checkout default branch
        # git pull
        pass
        #os.chdir(dep)
        ##subprocess.check_call(["git", "checkout", "main"])
        #subprocess.check_call(["git", "fetch", repo_url, dirname])

def checkout_new_branch(branch_name):
    pass

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("project")
    parser.add_argument("version_suffix")
    parser.add_argument("--base-git-uri")

    args = parser.parse_args()

    deps = read_dependencies("dependencies.json", args.project)
    owd = os.getcwd()
    for dep in deps:
        repo_url = f"{args.base_git_uri}{dep}.git"
        clone_repo(repo_url, dep)
        os.chdir(dep)
        # branch out
        branch_name = f"gitbot-update-{args.project}-{args.version_suffix}"
        subprocess.check_call(["git", "checkout", "-b", branch_name])
        # change include file
        subprocess.check_call(["sed", "-i", f"s/-[0-9]\+$/-{args.version_suffix}/", f"includes/{args.project}"])
        # commit & push
        subprocess.check_call(["git", "commit", f"includes/{args.project}", "-m", f"Update includes/{args.project} to {args.version_suffix}"])
        subprocess.check_call(["git", "push", "origin", branch_name])
        # open MR
